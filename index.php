<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

<div class="row">

<div class="col">
<?php
$xmlfile=$_GET["xml"];
if (file_exists($xmlfile))
{
	$proc=new XsltProcessor;
	$proc->importStylesheet(DOMDocument::load("xslfile.xsl")); //load XSL script
	echo $proc->transformToXML(DOMDocument::load($xmlfile)); //load XML file and echo

}
?>
</div>
<div class="col">
<!-- Latest compiled and minified CSS -->
<form action="add.php" method="POST">
<h2>Dodawanie nowego auta:</h2>
 <label for="marka">Marka:</label>
<input type="text" name="marka" class="form-control" id="marka"/>
<label for="model">Model:</label>
<input type="text" name="model" class="form-control" id="model"/>
<label for="typ">Typ:</label>
<input type="text" name="typ" class="form-control" id="typ"/>
<label for="pojemnosc">Pojemność:</label>
<input type="text" name="pojemnosc" class="form-control" id="pojemnosc"/>
<label for="rodzaj-paliwa">Rodzaj Paliwa:</label>
<input type="text" name="rodzaj-paliwa" class="form-control" id="rodzaj-paliwa"/>
<label for="skrzynia-biegow">Szkrzynia-biegów</label>
<input type="text" name="skrzynia-biegow" class="form-control" id="skrzynia-biegow"/>
<input type="hidden" name="xmlfile" value="<?php echo $xmlfile; ?>">
<br>
<input type="submit" value="Wyślij" class="btn btn-success">
</form>


</div>
</div>