<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:foo="http://www.foo.org/" xmlns:bar="http://www.bar.org">
<xsl:template match="/">
  <html>
  <body>
  <h4>Salon samochodowy:</h4>
    <table border="1">
      <tr bgcolor="#9acd32">
        <th>Marka</th>
        <th>Model</th>
        <th>Typ</th>
        <th>Pojemność</th>
        <th>Rodzaj Paliwa</th>
        <th>Skrzynia Biegow</th>
      </tr>
      <xsl:for-each select="salon/auto">
      <tr>
        <td><xsl:value-of select="podstawowe/marka"/></td>
        <td><xsl:value-of select="podstawowe/model"/></td>
        <td><xsl:value-of select="podstawowe/typ"/></td>
        <td><xsl:value-of select="naped/silnik/pojemnosc"/></td>
        <td><xsl:value-of select="naped/silnik/rodzaj-paliwa"/></td>
        <td><xsl:value-of select="naped/skrzynia-biegow"/></td>
      </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>