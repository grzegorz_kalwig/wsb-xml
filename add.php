<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">


<?php

	$xmlcontent = file_get_contents($_POST['xmlfile']);
	$salon = new SimpleXMLElement($xmlcontent);
	$auto = $salon->addChild('auto');
	$auto->addChild('podstawowe', '');
	$podstawowe = $auto->podstawowe->addChild('marka', $_POST['marka']);
	$podstawowe = $auto->podstawowe->addChild('model', $_POST['model']);
	$podstawowe = $auto->podstawowe->addChild('typ', $_POST['typ']);
	$auto->addChild('naped');
	$naped = $auto->naped->addChild('silnik');
	$naped = $auto->naped->silnik->addChild('pojemnosc', $_POST['pojemnosc']);
	$naped = $auto->naped->silnik->addChild('rodzaj-paliwa', $_POST['rodzaj-paliwa']);
	$naped = $auto->naped->addChild('skrzynia-biegow', $_POST['skrzynia-biegow']);
	
	$salon->asXML($_POST['xmlfile']);?>
	
	<div class="alert alert-success">
	<strong>Success!</strong> Poprawnie Dodano wpis do pliku <?php echo $_POST['xmlfile'];?>, <a href="index.php?xml=<?php echo $_POST['xmlfile'] ?>" class="alert-link">Kliknij tutaj aby wrócić</a>
	</div>
	
<?php
$xmlfile=$_POST['xmlfile'];
if (file_exists($xmlfile))
{
	$proc=new XsltProcessor;
	$proc->importStylesheet(DOMDocument::load("xslfile.xsl")); //load XSL script
	echo $proc->transformToXML(DOMDocument::load($xmlfile)); //load XML file and echo

}?>